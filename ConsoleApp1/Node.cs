﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Node<T>:INode<T> where T : IComparable<T>
    {
        public int Id { get; }
        T data = default(T);
        public T Data { get { return data; } }
        public Int32 Ves { get; set; }
        public bool Visited { get; set; }
        List<Node<T>> children;
        public Node<T> AddChild(Node<T> node, bool bidirect = true)
        {
            this.children.Add(node);
            if (bidirect)
            {
                node.children.Add(this);
            }
            return this;
        }
        public Node(int Id,T data)
        {
            this.Id = Id;
            this.data = data;
            //this.Distance = distance;
            this.children = new List<Node<T>>();
        }
        public bool ContainsChild(Node<T> node)
        {
            foreach (Node<T> n in children)
            {
                if (n.Id == node.Id && ((n.Data == null && node.Data == null)||n.Data.CompareTo(node.Data) == 0)) return true;
            }
            return false;
        }
    }
    
}
