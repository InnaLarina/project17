﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class RoadMap<City> :Graph<ConsoleApp1.City> where City : IComparable<City>, new()
    {
        //List<Road> roads;
        Int32[,] roads;
        const Int32 infinity = 2_147_483_647;
        Int32[] shortRoads;
        public RoadMap(string pathFile) : base() 
        {
            //roads = new List<Road>();
            int i = 0;
            string sb;
            string[] arr_cities = null;
            bool title = true;
            try
            {
              using (StreamReader sr = new StreamReader(pathFile))
              {
                    while (!sr.EndOfStream)
                    {
                        sb = sr.ReadLine();
                        if (i == 0&&title) 
                        {
                          arr_cities = sb.Split(';');
                           title = false; 
                          continue;
                        }
                        string[] arr = sb.Split(';');
                        if (i == 0) 
                        {
                            for (int j = 0; j < arr.Length; j++)
                            {
                              Node<ConsoleApp1.City> node = new Node<ConsoleApp1.City>(j,new ConsoleApp1.City(j, arr_cities[j]));
                              vertices.Add(node);
                            }
                        roads = new Int32[arr.Length, arr.Length];
                        shortRoads = new Int32[arr.Length];
                        }
                        Node<ConsoleApp1.City> nodeParent = this.GetData(i);
                        for (int j = 0; j < arr.Length; j++)
                        {
                            if (!String.IsNullOrEmpty(arr[j])) 
                            {
                               Node<ConsoleApp1.City> nodeChild = this.GetData(j);
                               if(!nodeParent.ContainsChild(nodeChild)) nodeParent.AddChild(nodeChild);
                               roads[i,j] = Int32.Parse(arr[j]);
                        }
                        }
                        i++;
                    }
              }
           }
            catch (Exception e) 
            { 
              Console.WriteLine(e.Message);
            }
        }
        
        void firstStepDijkstra(int id) 
        {
            if (id >= vertices.Count || id < 0) return;
            foreach (Node<ConsoleApp1.City> node in vertices)
            {
                if (node.Id == id) node.Ves = 0; else node.Ves = infinity;
                node.Visited = false;
                shortRoads[node.Id] = id;
            }
        }
        bool IsThereNotVisitedToReach()
        {
            foreach (Node<ConsoleApp1.City> node in vertices)
            {
                if (!node.Visited && node.Ves < infinity) return true;
            }
            return false;
        }
        Node<ConsoleApp1.City> MinVesNode() 
        {
            Int32 minVes = infinity;
            Node<ConsoleApp1.City> nodeMinVes = null;
            foreach (Node<ConsoleApp1.City> node in vertices)
            {
                if (!node.Visited&&node.Ves < minVes) 
                {
                    minVes = node.Ves;
                    nodeMinVes = node;
                }
            }
            return nodeMinVes;
        }
        List<Node<ConsoleApp1.City>> Neighbours(Node<ConsoleApp1.City> node) 
        {
            List<Node<ConsoleApp1.City>> result = new List<Node<ConsoleApp1.City>>();
            int i = node.Id;
            Node<ConsoleApp1.City> curNode;
            for (int j = 0; j < vertices.Count; j++)
            {
                if (roads[i, j] != 0) 
                {
                    curNode = this.GetData(j);
                    if (!curNode.Visited) result.Add(curNode);
                }
            }
            return result;
        }
        public void Dijkstra(int id) 
        {
            firstStepDijkstra(id);
            while (IsThereNotVisitedToReach())
            {
                Node<ConsoleApp1.City> nodeWork = this.MinVesNode();
                List<Node<ConsoleApp1.City>> neighbours = this.Neighbours(nodeWork);
                foreach (Node<ConsoleApp1.City> node in neighbours) 
                {
                    if (nodeWork.Ves + roads[nodeWork.Id, node.Id] < node.Ves) 
                    {
                        node.Ves = nodeWork.Ves + roads[nodeWork.Id, node.Id];
                        shortRoads[node.Id] = nodeWork.Id;
                    }
                }
                nodeWork.Visited = true;
            }
        }
        public string ShortRoad(int startNode,int destNode) 
        {
            string result = "";
            int lengthWay = 0;
            int oldInt = 0;
            Stack<int> inds = new Stack<int>();
            inds.Push(destNode);
            //string gorod = vertices.
            result += $"The shortest way from {this.GetData(startNode).Data.NameCity} to {this.GetData(destNode).Data.NameCity}:" + Environment.NewLine;
            if (shortRoads[destNode] == startNode && roads[startNode, destNode] == 0) 
            {
                result += "No way" + Environment.NewLine;
                lengthWay = infinity;
                return result;
            }
            Int32 curInd = destNode;
            while (curInd != startNode) 
            {
                oldInt = curInd;
                curInd = shortRoads[curInd];
                lengthWay += roads[curInd, oldInt];
                inds.Push(curInd);
            }
            while (inds.Count!=0)
            {
                result += $" {this.GetData(inds.Pop()).Data.NameCity}, ";
            }
            result = result.TrimEnd(new char[2] {' ',','});
            result += $" equals {lengthWay} km.";
            return result;
        }
        public string ThereAreCities() 
        {
            string result = "There are cities: ";
            foreach (Node<ConsoleApp1.City> node in vertices)
            {
                result += node.Data.Id.ToString() + " " + node.Data.NameCity + ", ";
            }
            result = result.TrimEnd(new char[2] { ' ', ',' });
            return result;
        }
        public string ToMatrix()
        {
            string result = "";
            foreach (Node<ConsoleApp1.City> node in vertices)
            {
                result += node.Data.NameCity + ";";
            }
            result = result.TrimEnd(';');
            result = result + Environment.NewLine;
            int rows = roads.GetUpperBound(0) + 1;
            int columns = roads.GetUpperBound(1) + 1;
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    result += $"{roads[i, j]};";
                }
                result = result.TrimEnd(';');
                result = result + Environment.NewLine;
            }
            return result;
        }

    }
}
