﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Graph<T> where T : IComparable<T>

    {
        //public Dictionary<int, Node<T>> vertices;
        public List<Node<T>> vertices;
        public Graph() 
        {
            vertices = new List<Node<T>>();
        }
        void AddNode(T data)
        {
            if (!this.Contains(data)) 
            { 
              int iniKey = this.maxKey() + 1;
              Node<T> node = new Node<T>(iniKey, data);
              vertices.Add(node);
            }
        }
        int maxKey() 
        {
            int result = 0;
            foreach (Node<T> node in vertices)
            {
                if (result > node.Id) result = node.Id;
            }
            return result;
        }
        bool Contains(T data)
        {
            bool result = false;
            foreach (Node<T> node in vertices)
            {
                if (node.Data.CompareTo(data) == 0) return true;
            }
            return result;
        }
        void RemoveNode(T data) 
        {
            Node<T> nodeToRemove = null;
            if (!this.Contains(data)) return;
            foreach (Node<T> node in vertices)
            {
                if (node.Data.CompareTo(data) == 0) 
                {
                    nodeToRemove = node;
                }
            }
            vertices.Remove(nodeToRemove);
        }
        public Node<T> GetData(int Id) 
        {
            foreach (Node<T> node in vertices)
            {
                if (node.Id == Id) return node;
            }
            return null;
        }
        int GetId(T data) 
        {
            if (!this.Contains(data)) return -1;
            foreach (Node<T> node in vertices)
            {
                if (node.Data.CompareTo(data) == 0) return node.Id;
            }
            return -1;
        }
    }
}
