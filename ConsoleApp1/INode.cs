﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    interface INode<T> where T : IComparable<T>
    {
        /*void Add(T data);
        void Add(int Id);
        bool Contains(T data);
        void Remove(T data);
        void Remove(int Id);
        T GetT(int Id);*/
        Node<T> AddChild(Node<T> node, bool bidirect = true);
        bool ContainsChild(Node<T> node); 
    }
}
