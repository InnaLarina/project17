﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public struct Road
    {
        public int Id1;
        public int Id2;
        public double Distance;
    }
}
