﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class City:IComparable<City>
    {
        public int Id {get;set;}
        public string NameCity { get; set; }
        public City(int Id, string NameCity) 
        {
            this.Id = Id;
            this.NameCity = NameCity;
        }
        public City()
        {
            this.Id = 0;
            this.NameCity = "";
        }
        public override string ToString()
        {
            return this.NameCity;
        }

        public int CompareTo(City city)
        {
            if (this.NameCity == city.NameCity && this.Id == city.Id) return 0; else return 1;
        }
    }
}
