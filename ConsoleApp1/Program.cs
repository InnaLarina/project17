﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            try 
            {
                RoadMap<ConsoleApp1.City> roadMap = new RoadMap<ConsoleApp1.City>(@"D:\C#Education\Git\Graph\ConsoleApp1\graph8_unlinked_cities.txt");
                Console.WriteLine(roadMap.ThereAreCities());
                Console.WriteLine();
                Console.WriteLine("Serialization:");
                Console.WriteLine(roadMap.ToMatrix());
                Console.WriteLine("Choose the number of a city between 0 and " + (roadMap.vertices.Count - 1).ToString()+" to build the way from");
                int start; 
                string strStart = Console.ReadLine();
                start = Int32.Parse(strStart);
                if (start >= roadMap.vertices.Count || start < 0) 
                {
                    Console.WriteLine("Bad choice");
                    Console.ReadKey();
                    return;
                }
                roadMap.Dijkstra(start);
                Console.WriteLine("Choose the number of a city between 0 and " + (roadMap.vertices.Count - 1).ToString() + " to build the way to");
                int finish;
                string strFinish = Console.ReadLine();
                finish = Int32.Parse(strFinish);
                if (finish >= roadMap.vertices.Count || finish < 0)
                {
                    Console.WriteLine("Bad choice");
                    Console.ReadKey();
                    return;
                }
                Console.WriteLine(roadMap.ShortRoad(start, finish));
                Console.ReadKey(); 
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
    }
}
